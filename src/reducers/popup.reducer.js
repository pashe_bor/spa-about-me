import {
  IMAGE_POPUP_OPEN,
  IMAGE_POPUP_CLOSE
} from '../constants'

const initialState = {
  isPopupOpen: false,
  image: ''
};

export const popupReducer = (state = initialState, action) => {
  switch (action.type) {
    case IMAGE_POPUP_OPEN:
      return Object.assign({}, state, {
        isPopupOpen: action.payload.isOpen,
        image: action.payload.image
      })
    case IMAGE_POPUP_CLOSE:
      return Object.assign({}, state, {
        isPopupOpen: action.payload,
        image: action.payload
      })
  }
  return state;
};