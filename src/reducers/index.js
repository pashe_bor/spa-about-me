import { combineReducers } from 'redux'

import {popupReducer} from './popup.reducer'

const reducers = combineReducers({
  popupReducer: popupReducer
})

export default reducers