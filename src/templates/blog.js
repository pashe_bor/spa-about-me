import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import FlyContainer from '../elements/containers/FlyContainer'
import TitleImage from './../elements/image-elements/TitleImage'
import BlogList from './../components/blog/BlogList'
import Paginator from '../components/blog/paginator/Paginator'
import favicon from './../assets/images/favicon.png'

export const blogMetaQuery = graphql`
query blogQuery {
    site {
      siteMetadata {
        keywords
        blog {
          title
          description
        }
      }
    }
 }
`

class BlogTemplate extends Component {
  constructor() {
    super()
    this.posts = ''
    this.metaData = ''
  }
  componentWillMount() {
    this.posts = this.props.pathContext.group;
    this.metaData = this.props.data.site.siteMetadata
  }

  render() {
    return (
      <FlyContainer>
        <Helmet
          title={this.metaData.blog.title}
          meta={[
            { name: 'description', content: this.metaData.blog.description },
            { name: 'keywords', content: this.metaData.keywords },
            { name: 'theme-color', content: '#881F62'},
            { name: 'background', content: '#881F62'},
            { name: 'lang', content: 'ru'}
          ]}
          link={[
            { rel: 'shortcut icon', type: 'image/png', href: `${favicon}` }
          ]}
        />
        <TitleImage type={'blog'} />
        <h1 className={'main-header'}>Блог</h1>
        <BlogList posts={this.posts} />
        <Paginator pathContext={this.props.pathContext}/>
      </FlyContainer>
    )
  }
}

BlogTemplate.propTypes = {
  data: PropTypes.object.isRequired,
  edges: PropTypes.array,
}

export default BlogTemplate
