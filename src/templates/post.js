import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Img from 'gatsby-image'
import Link from 'gatsby-link'
import FlyContainer from '../elements/containers/FlyContainer'
import TitleImage from '../elements/image-elements/TitleImage'
import favicon from './../assets/images/favicon.png'
import Helmet from 'react-helmet'

class PostTemplate extends Component {
  render() {
    const post = this.props.data.wordpressPost
    const postImage = this.props.data.wordpressPost.featured_media.localFile
      .childImageSharp.resolutions,
          siteMeta = this.props.data.site.siteMetadata.description
    return (
      <FlyContainer>
        <Helmet
          title={`Павел Демьянов | ${post.title}`}
          meta={[
            { name: 'keywords', content: siteMeta },
            { name: 'theme-color', content: '#881F62'},
            { name: 'background', content: '#881F62'},
            { name: 'lang', content: 'ru'}
          ]}
          link={[
            { rel: 'shortcut icon', type: 'image/png', href: `${favicon}` }
          ]}
        />
        <TitleImage type={'blog-post'} />
        <div className="blog-post">
          <Link
            className={'blog-post__back-link'}
            title={'К списку статей'}
            to={'/blog/'}
          >
            <i className={'material-icons'}>arrow_back_ios</i>
          </Link>
          <h1
            className={'main-header'}
            dangerouslySetInnerHTML={{ __html: post.title }}
          />
          <Img className={'blog-post__image'} resolutions={postImage} />
          <article
            className={'blog-post-content'}
            dangerouslySetInnerHTML={{ __html: post.content }}
          />
        </div>
      </FlyContainer>
    )
  }
}

export default PostTemplate

export const pageQuery = graphql`
  query currentPostQuery($id: String!) {
    wordpressPost(id: { eq: $id }) {
      title
      content
      excerpt
      featured_media {
        localFile {
          childImageSharp {
            resolutions(width: 600, height: 300, quality: 50) {
              src
              srcSet
              width
              height
            }
          }
        }
      }
    }
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`
