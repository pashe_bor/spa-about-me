import React from 'react'

const FlyContainer = ({ children, styles }) => (
    <main className="main" style={styles}>
          <div className="container container--fly" style={{height: 'auto'}}>{children}</div>
    </main>
)

export default FlyContainer
