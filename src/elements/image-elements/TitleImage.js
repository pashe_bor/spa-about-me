import React from 'react'
import userImage from './../../assets/images/smile.jpg'

const TitleImage = ({ type }) => {
  const imageType = () => {
    switch (type) {
      case 'blog':
        return <i className="rounded-circle material-icons">library_books</i>
        break
      case 'portfolio':
        return <i className="rounded-circle material-icons">computer</i>
        break
      case 'blog-post':
        return <i className="rounded-circle material-icons">view_headline</i>
        break
      case '404':
        return <i className="rounded-circle material-icons">error_outline</i>
        break
      default:
        return (
          <img className={'rounded-circle'} src={userImage} alt={userImage} />
        )
    }
  }
  return <figure className={'title-image'}>{imageType()}</figure>
}

export default TitleImage
