import React from 'react'

const PreviewButton = ({ children, click, isActive }) => {
  return (
    <button
      type={'button'}
      onClick={click}
      className={`button button-preview ${
        isActive ? 'button-preview--active' : ''
      }`}
    >
      {children}
    </button>
  )
}

export default PreviewButton
