import React from 'react'

const ImageViewPopup = ({ image, closePopup }) => {
  return (
    <section className={'popup-overlay'} onClick={closePopup}>
      <div className="popup">
        <div className={'popup-close'}>
          <i className={'material-icons'}>clear</i>
        </div>
        <img src={image} draggable={true} />
      </div>
    </section>
  )
}

export default ImageViewPopup
