import React from 'react'

const Tags = ({click}) => {
  return (
    <div className="tags">
      <p className={'tags-filter'}>Филтьтр технологий:
        <i className={'fi fi-react tags-filter__item'} data-tag="react" onClick={click}> </i>
        <i className={'fi fi-vuejs tags-filter__item'} data-tag="vuejs" onClick={click}> </i>
        <i className={'fi fi-nodejs tags-filter__item'} data-tag="nodejs" onClick={click}> </i>
        <i className={'fi fi-python tags-filter__item'} data-tag="python" onClick={click}> </i>
        <i className={'fi fi-php tags-filter__item'} data-tag="php" onClick={click}> </i>
        <i className={'fi fi-wordpress tags-filter__item'} data-tag="wordpress" onClick={click}> </i>
        <i className={'fi fi-android tags-filter__item'} data-tag="android" onClick={click}> </i>
        <i className={'bitrix-icon tags-filter__item'} data-tag="bitrix" onClick={click}> </i>
        <i className={'django-icon tags-filter__item'} data-tag="django" onClick={click}> </i>
        <i className={'express-icon tags-filter__item'} data-tag="express" onClick={click}> </i>
        <i className={'modx-icon tags-filter__item'} data-tag="modx" onClick={click}> </i>
      </p>
    </div>
  )
}

export default Tags