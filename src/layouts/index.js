import React from 'react'
import LayoutContainer from '../components/LayoutContainer'
import './index.scss'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import middleWare from 'redux-thunk'
import reducers from '../reducers';
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import "prismjs/themes/prism-solarizedlight.css";



const store = createStore(reducers, {}, applyMiddleware(middleWare));

class Layout extends React.Component {
 render() {
   return (
     <Provider store={store}>
       <LayoutContainer data={this.props}/>
     </Provider>
   )
 }
}

export default Layout

export const query = graphql`
  query SiteTitleQ {
    site {
      siteMetadata {
        title
        keywords
        description
      }
    }
  }
`
