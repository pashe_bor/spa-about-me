---
author: "pashebor"
description: "Отображает всю аналитику по яндекс метрике(yandex API) интернет-ресурса клиента, а так же выводит промежуточные результаты аналитик из google spreadsheets. Имеется функция вывода отчетов, формирования оных, тикет-чат с менеджером проекта и т.д. Использовал React + typescript + redux"
imagesThumb: [./images/sg-user-app/sg-analitics.png, 
         ./images/sg-user-app/sg-analytics2.png,
         ./images/sg-user-app/sg-documents.png,
         ./images/sg-user-app/sg-metrics.png,
         ./images/sg-user-app/sg-pm.png,
         ./images/sg-user-app/sg-reports.png,]
imagesFull: [./images/sg-user-app/sg-analitics.png, 
                      ./images/sg-user-app/sg-analytics2.png,
                      ./images/sg-user-app/sg-documents.png,
                      ./images/sg-user-app/sg-metrics.png,
                      ./images/sg-user-app/sg-pm.png,
                      ./images/sg-user-app/sg-reports.png,]         
tags: ["react", "bitrix", "php"]
appName: 'Личный кабинет клиента компании "Генератор продаж"'
section: 'portfolio'
---