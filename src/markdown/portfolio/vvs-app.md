---
author: "pashebor"
description: "Список поставщиков для всех направлений бизнеса торговли. Front-end: react + redux; Back-end: php7"
imagesThumb: [./images/vvs-app/vvs-report2.png, 
         ./images/vvs-app/vvs-report3.png,
         ./images/vvs-app/vvs-report-show.png,
         ./images/vvs-app/vvs-reports-adm.png,
         ./images/vvs-app/vvs-user.png,
         ./images/vvs-app/vvs-users.png,]
imagesFull: [./images/vvs-app/vvs-report2.png, 
                     ./images/vvs-app/vvs-report3.png,
                     ./images/vvs-app/vvs-report-show.png,
                     ./images/vvs-app/vvs-reports-adm.png,
                     ./images/vvs-app/vvs-user.png,
                     ./images/vvs-app/vvs-users.png,]         
tags: ["react", "php"]
appName: 'VVS - таблицы поставщиков'
section: 'portfolio'
---