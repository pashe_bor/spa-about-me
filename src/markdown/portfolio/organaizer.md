---
author: "pashebor"
description: "Приложение сделал для своих собственных нужд, по фану. Оно содержит: чат на socket.io, конвертер валют и, естественно, заметки('TodoList') со степенью важности. Знакомился с тейнологией  Vue.js + Vuex"
imagesThumb: [./images/organaizer/todo1.png, 
         ./images/organaizer/todo2.png,
         ./images/organaizer/todo3.png,
         ./images/organaizer/todo4.png,
         ./images/organaizer/todo5.png,
         ./images/organaizer/todo6.png,]
imagesFull: [./images/organaizer/todo1.png, 
         ./images/organaizer/todo2.png,
         ./images/organaizer/todo3.png,
         ./images/organaizer/todo4.png,
         ./images/organaizer/todo5.png,
         ./images/organaizer/todo6.png,]         
tags: ["vuejs", "nodejs", "express"]
appName: 'Огранайзер'
section: 'portfolio'
---