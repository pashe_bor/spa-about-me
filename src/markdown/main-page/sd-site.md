---
author: "pashebor"
imagesThumb: [./images/sd-site/slovo1.png, 
         ./images/sd-site/slovo2.png,
         ./images/sd-site/slovo3.png,
         ./images/sd-site/slovo4.png,
         ./images/sd-site/slovo5.png,
         ./images/sd-site/slovo7.png,]
imagesFull: [./images/sd-site/slovo1.png, 
                     ./images/sd-site/slovo2.png,
                     ./images/sd-site/slovo3.png,
                     ./images/sd-site/slovo4.png,
                     ./images/sd-site/slovo5.png,
                     ./images/sd-site/slovo7.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---