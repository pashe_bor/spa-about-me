---
author: "pashebor"
imagesThumb: [./images/cosmo-site/main-all.png, 
         ./images/cosmo-site/cosmo-main.png,
         ./images/cosmo-site/main-all.png,
         ./images/cosmo-site/cosmo-main.png,]
imagesFull: [./images/cosmo-site/main-all.png, 
                     ./images/cosmo-site/cosmo-main.png,
                     ./images/cosmo-site/main-all.png,
                     ./images/cosmo-site/cosmo-main.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---