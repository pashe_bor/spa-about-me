---
author: "pashebor"
imagesThumb: [./images/medka-site/medka1.png, 
         ./images/medka-site/medka2.png,
         ./images/medka-site/medka3.png,
         ./images/medka-site/medka4.png,
         ./images/medka-site/medka5.png,]
imagesFull: [./images/medka-site/medka1.png, 
                     ./images/medka-site/medka2.png,
                     ./images/medka-site/medka3.png,
                     ./images/medka-site/medka4.png,
                     ./images/medka-site/medka5.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---