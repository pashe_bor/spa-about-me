---
author: "pashebor"
imagesThumb: [./images/myremont-site/myrem1.png, 
         ./images/myremont-site/myrem2.png,
         ./images/myremont-site/myrem3.png,
         ./images/myremont-site/myrem4.png,]
imagesFull: [./images/myremont-site/myrem1.png, 
                     ./images/myremont-site/myrem2.png,
                     ./images/myremont-site/myrem3.png,
                     ./images/myremont-site/myrem4.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---