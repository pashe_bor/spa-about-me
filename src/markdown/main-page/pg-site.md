---
author: "pashebor"
imagesThumb: [./images/pg-site/pg1.png, 
         ./images/pg-site/pg2.png,
         ./images/pg-site/pg3.png,
         ./images/pg-site/pg4.png,
         ./images/pg-site/pg5.png,]
imagesFull: [./images/pg-site/pg1.png, 
                     ./images/pg-site/pg2.png,
                     ./images/pg-site/pg3.png,
                     ./images/pg-site/pg4.png,
                     ./images/pg-site/pg5.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---