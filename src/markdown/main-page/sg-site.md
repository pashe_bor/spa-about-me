---
author: "pashebor"
imagesThumb: [./images/sg-site/screencapture-sales-generator-ru-2018-07-29-18_16_.png, 
         ./images/sg-site/Screenshot_2018-07-29_18-29-42.png,
         ./images/sg-site/screencapture-sales-generator-ru-company-2018-07-2.png,
         ./images/sg-site/Screenshot_2018-07-29_18-31-22.png,]
imagesFull: [./images/sg-site/screencapture-sales-generator-ru-2018-07-29-18_16_.png, 
                     ./images/sg-site/Screenshot_2018-07-29_18-29-42.png,
                     ./images/sg-site/screencapture-sales-generator-ru-company-2018-07-2.png,
                     ./images/sg-site/Screenshot_2018-07-29_18-31-22.png,]         
tags: ["vuejs", "nodejs", "express"]
section: 'main-slider'
---