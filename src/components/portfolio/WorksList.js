import React from 'react'
import { brands } from './../content/images/images'
import Slider from 'react-slick'
import Tags from './../../elements/tags-list/Tags'
import {openImagePopupAction} from './../../actions'
import {bindActionCreators} from "redux"
import {connect} from "react-redux"

class WorksList extends React.Component {
  componentDidMount() {
    console.log(this.props)
  }

  tagClickHandler(event) {
    console.log(event.target.getAttribute('data-tag'))
  }
  
  workImageClickHandler(event) {
    const fullImage = event.target.getAttribute('data-full-image')
    this.props.openImagePopupAction({isOpen: true, image: fullImage})
  }
  
  render() {
    const worksData = this.props['works-data'];
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 4,
      centerMode: false,
      centerPadding: "20px",
      arrows: false,
      lazyLoad: 'ondemand',
      autoplay: true,
      autoplaySpeed: 5000,
      responsive: [{
        breakpoint: 768,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 2,
          slidesToScroll: 2,
          centerMode: false,
          centerPadding: "20px",
          arrows: false,
          lazyLoad: 'ondemand',
          autoplay: true,
          autoplaySpeed: 5000,
        }
      }]
    }, worksSettings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 3,
      centerMode: false,
      centerPadding: "20px",
      arrows: true,
      lazyLoad: 'ondemand',
      autoplay: false,
      autoplaySpeed: 5000,
      responsive: [{
        breakpoint: 768,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 2,
          slidesToScroll: 2,
          centerMode: false,
          centerPadding: "20px",
          arrows: false,
          lazyLoad: 'ondemand',
          autoplay: false,
          autoplaySpeed: 5000,
        }
      }]
    }
    
    
    return (
      <section className={'portfolio'}>
        <Tags click={this.tagClickHandler.bind(this)}/>
        <section className={'work-data'}>
        {worksData.map((work, key) => {
          if (work.node.frontmatter.section === 'portfolio') {
            return (
              <div className={'work-data__item'} key={key}>
                <h5>{work.node.frontmatter.appName}</h5>
                <div className="content">
                  <blockquote>Технологии: {work.node.frontmatter.tags.map((tag, key) => {
                    if (tag !== 'bitrix' &&
                      tag !== 'django' &&
                      tag !== 'express' &&
                      tag !== 'modx') {
                      return <i key={key} className={`fi fi-${tag} tags-filter__item`} data-tag={tag}> </i>
                    } else {
                      return <i className={`${tag}-icon tags-filter__item`} key={key} data-tag={tag}> </i>
                    }
                  })}
                  </blockquote>
                  <p>{work.node.frontmatter.description}</p>
                </div>
                <Slider {...worksSettings}>
                  {work.node.frontmatter.imagesThumb.map((thumb, i) => {
                    return <img src={thumb.childImageSharp.resolutions.src}
                                data-full-image={work.node.frontmatter.imagesFull[i].childImageSharp.resolutions.src}
                                key={i}
                                onClick={this.workImageClickHandler.bind(this)}/>
                  })}
                </Slider>
              </div>
            )
          }
        })}
        </section>
        <h5>Работал с компаниями:</h5>
        <div className={'portfolio-brands'}>
          <Slider {...settings}>
            {brands.map((brand, i) => <div key={i} className={'portfolio-brands__item'}><img src={brand}/></div>)}
          </Slider>
        </div>
      </section>
    )
  }
}

export function mapStateToProps(store) {
  return {}
}

export const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({openImagePopupAction}, dispatch)
};

export default  connect(mapStateToProps, mapDispatchToProps)(WorksList);