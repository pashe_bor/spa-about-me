import React from 'react'
import NavLinks from './../links/NavLinks'

const NavMenuBottom = ({ location }) => (
  <nav className={'navigation-bottom'}>
    <NavLinks path={location.pathname} />
  </nav>
)

export default NavMenuBottom
