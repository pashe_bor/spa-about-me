import React from 'react'
import Link from 'gatsby-link'
import NavLinks from './../links/NavLinks'
import ContactLinks from './../links/ContactLinks'

class NavMenu extends React.Component {
  constructor() {
    super()
    this.state = {
      hamburgerIsActive: false
    }
  }

  mobileMenuOpener() {
    if(this.state.hamburgerIsActive) {
      this.setState({hamburgerIsActive: false})
    } else {
      this.setState({hamburgerIsActive: true})
    }
  }
  render() {
    return(
      <nav
        className={
          this.props.scrollPosition > 100 ? 'navigation navigation--scrolled' : 'navigation'
        }
      >
        <div className="container">
          <div className="logo">
            <Link to={'/'}>Pashebor's Laba</Link>
          </div>
          <NavLinks path={this.props.location.pathname} scrolled={this.props.scrollPosition > 100} />
          <ContactLinks scrolled={this.props.scrollPosition> 100} isHeader={true}/>
          {/*Mobile menu*/}
          <button className={`hamburger hamburger--squeeze ${this.state.hamburgerIsActive ? 'is-active' : ''}`}
                  type="button"
                  aria-label="Menu"
                  aria-controls="navigation"
                  onClick={this.mobileMenuOpener.bind(this)}
          >
              <span className="hamburger-box">
              <span className="hamburger-inner"></span>
              </span>
          </button>
          <div className={`mobile-menu ${this.state.hamburgerIsActive ? 'mobile-menu--is-open' : ''}`}>
            <NavLinks path={this.props.location.pathname} scrolled={this.props.scrollPosition > 100} />
            <ContactLinks scrolled={this.props.scrollPosition > 100} />
          </div>
        </div>
      </nav>
    )
  }
}

export default NavMenu
