import React from 'react'
import mainBackgroundImage from './../assets/images/main-bg-image.jpg'
import blogBackgroundImage from './../assets/images/blog.jpg'
import portfolioBackgroundImage from './../assets/images/portfolio.jpg'
import { Parallax } from 'react-parallax'
import NavMenu from './nav-menu/NavMenu'

class Header extends React.Component {
  constructor() {
    super()
    this.state = { windowPosition: 0 }
  }

  componentDidMount() {
    let that = this
    const windObj = window

    windObj.addEventListener('scroll', function() {
      that.setState({ windowPosition: this.pageYOffset })
    })
  }

  render() {
    const setBackgroundImage = () => {
      switch (this.props.location.pathname) {
        case '/':
          return (
            <Parallax strength={200} bgImage={mainBackgroundImage}>
              <div style={{ height: 380 }} />
            </Parallax>
          )
          break
        case '/blog/':
          return (
            <Parallax strength={200} bgImage={blogBackgroundImage}>
              <div style={{ height: 380 }} />
            </Parallax>
          )
          break
        case '/portfolio/':
          return (
            <Parallax strength={200} bgImage={portfolioBackgroundImage}>
              <div style={{ height: 380 }} />
            </Parallax>
          )
          break
        default:
          return (
            <Parallax strength={200} bgImage={mainBackgroundImage}>
              <div style={{ height: 380 }} />
            </Parallax>
          )
      }
    }
    return (
      <header>
        {setBackgroundImage()}
        <NavMenu
          location={this.props.location}
          scrollPosition={this.state.windowPosition}
        />
      </header>
    )
  }
}

export default Header
