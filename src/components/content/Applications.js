import React from 'react'
import { applications } from './images/images'
import Work from './Work'

class Applications extends Work {
  constructor(props) {
    super(props).sliderBlocks = applications
  }

  render() {
    return <div>{this.setSlider(2)}</div>
  }
}

export default Applications
