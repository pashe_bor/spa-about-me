import React from 'react'
import { other } from './images/images'
import Work from './Work'

class Favourite extends Work {
  constructor(props) {
    super(props).sliderBlocks = other
  }
  render() {
    return <div>{this.setSlider(2)}</div>
  }
}

export default Favourite
