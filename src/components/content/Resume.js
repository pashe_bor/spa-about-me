import React from 'react'

const Resume = () => (
  <section className={'preview'}>
    <h5>Хобби:</h5>
    <ul>
      <li>Современное искусство</li>
      <li>Кинематограф</li>
      <li>Книги</li>
    </ul>
    <h5>Рзаработка сайтов на CMS таких как:</h5>
    <ul>
      <li>Bitrix</li>
      <li>Wordpress</li>
      <li>Modx</li>
    </ul>
    <p>
      Из фронтенд фреймворков предпочитаю React за его коммьюнити, далее по
      списку Vue. Back-end: NodeJS(Express), python(Flask, Django),
      php(Symphony) & object-oriented.
    </p>
  </section>
)

export default Resume
