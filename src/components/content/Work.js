import React from 'react'
import { sites } from './images/images'
import { chunkifyArray } from '../../utils/helpers'
import Slider from 'react-slick'

class Work extends React.Component {
  constructor(props) {
    super(props)
    this.popupAction = this.props.popupAction
    this.sliderBlocks = sites
  }

  imageClickHandler(image) {
    this.popupAction(image)
  }
  //Setting slider nested blocks
  setSliderBlocks(columns) {
    const divideIntoItems = block => {
      let imagesArray = []

      for (let image in block) {
        imagesArray.push(block[image])
      }
      //Split an images array into columns
      const splittedArray = chunkifyArray(imagesArray, columns, true)
      //Output preview block items with images
      if (columns > 1) {
        return splittedArray.map((item, i) => {
          return (
            <div key={i} className={'preview-block__item'}>
              {item.map((image, i) => {
                return (
                  <img
                    onClick={this.imageClickHandler.bind(this, image.full)}
                    className={'image'}
                    key={i}
                    src={image.thumb}
                    alt={image.thumb}
                  />
                )
              })}
            </div>
          )
        })
      }
    }
    //Setting slides
    return this.sliderBlocks.map((block, i) => {
      return (
        <div key={i}>
          <div className={'preview-block'}>
            {divideIntoItems(block)}
          </div>
        </div>
      )
    })
  }

  setSlider(rows) {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      lazyLoad: 'ondemand',
      autoplay: false,
      responsive: [{
        breakpoint: 768,
        settings: {
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          lazyLoad: 'ondemand',
          autoplay: false,
          autoplaySpeed: 5000,
        }
      }]
    }
    return (
      <Slider {...settings}>
        {this.setSliderBlocks(rows)}
      </Slider>
    )
  }

  render() {
    return <div>{this.setSlider(2)}</div>
  }
}
export default Work
