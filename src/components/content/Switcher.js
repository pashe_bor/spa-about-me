import React from 'react'
import PreviewButton from '../../elements/buttons/PreviewButton'

const Switcher = ({ state, previewSwitcher }) => (
  <section className={'button-group'}>
    <PreviewButton
      isActive={state.previews.works}
      click={() => previewSwitcher('works')}
    >
      <i className={'material-icons'}>work</i>
      <span>Сайты</span>
    </PreviewButton>
    <PreviewButton
      isActive={state.previews.applications}
      click={() => previewSwitcher('applications')}
    >
      <i className={'material-icons'}>favorite</i>
      <span>Приложения</span>
    </PreviewButton>
    <PreviewButton
      isActive={state.previews.other}
      click={() => previewSwitcher('other')}
    >
      <i className={'material-icons'}>devices_other</i>
      <span>Прочее</span>
    </PreviewButton>
    <PreviewButton
      isActive={state.previews.resume}
      click={() => previewSwitcher('resume')}
    >
      <i className={'material-icons'}>assignment_ind</i>
      <span>Резюме</span>
    </PreviewButton>
  </section>
)

export default Switcher
