import React from 'react'
/*Applications Start*/
import sgAppAnalitycs from '../../../assets/images/works/sg/thumb/sg-analitics.png'
import sgAppAnalitycsFull from '../../../assets/images/works/sg/sg-analitics.png'
import sgAppAnalitycs2 from '../../../assets/images/works/sg/thumb/sg-analytics2.png'
import sgAppAnalitycs2Full from '../../../assets/images/works/sg/sg-analytics2.png'
import sgAppDocuments from '../../../assets/images/works/sg/thumb/sg-documents.png'
import sgAppDocumentsFull from '../../../assets/images/works/sg/sg-documents.png'
import sgAppMetrix from '../../../assets/images/works/sg/thumb/sg-metrics.png'
import sgAppMetrixFull from '../../../assets/images/works/sg/sg-metrics.png'
import sgAppPm from '../../../assets/images/works/sg/thumb/sg-pm.png'
import sgAppPmFull from '../../../assets/images/works/sg/sg-pm.png'
import sgAppReports from '../../../assets/images/works/sg/thumb/sg-reports.png'
import sgAppReportsFull from '../../../assets/images/works/sg/sg-reports.png'
import vvsApp1 from '../../../assets/images/works/vvs/thumb/vvs-report-show.png'
import vvsApp1Full from '../../../assets/images/works/vvs/vvs-report-show.png'
import vvsApp2 from '../../../assets/images/works/vvs/thumb/vvs-report2.png'
import vvsApp2Full from '../../../assets/images/works/vvs/vvs-report2.png'
import vvsApp3 from '../../../assets/images/works/vvs/thumb/vvs-report3.png'
import vvsApp3Full from '../../../assets/images/works/vvs/vvs-report3.png'
import vvsApp4 from '../../../assets/images/works/vvs/thumb/vvs-users.png'
import vvsApp4Full from '../../../assets/images/works/vvs/vvs-users.png'
import vvsApp5 from '../../../assets/images/works/vvs/thumb/vvs-reports-adm.png'
import vvsApp5Full from '../../../assets/images/works/vvs/vvs-reports-adm.png'
import vvsApp6 from '../../../assets/images/works/vvs/thumb/vvs-user.png'
import vvsApp6Full from '../../../assets/images/works/vvs/vvs-user.png'
import todo1 from '../../../assets/images/works/todo/thumb/todo1.png'
import todo1Full from '../../../assets/images/works/todo/todo1.png'
import todo2 from '../../../assets/images/works/todo/thumb/todo2.png'
import todo2Full from '../../../assets/images/works/todo/todo2.png'
import todo3 from '../../../assets/images/works/todo/thumb/todo3.png'
import todo3Full from '../../../assets/images/works/todo/todo3.png'
import todo4 from '../../../assets/images/works/todo/thumb/todo4.png'
import todo4Full from '../../../assets/images/works/todo/todo4.png'
import todo5 from '../../../assets/images/works/todo/thumb/todo5.png'
import todo5Full from '../../../assets/images/works/todo/todo5.png'
import todo6 from '../../../assets/images/works/todo/thumb/todo6.png'
import todo6Full from '../../../assets/images/works/todo/todo6.png'
/*Applications End*/
/*Other Start*/
import gift1 from '../../../assets/images/other/thumb/gift1.png'
import gift1Full from '../../../assets/images/other/gift1.png'
import gift2 from '../../../assets/images/other/thumb/gift2.png'
import gift2Full from '../../../assets/images/other/gift2.png'
import land1 from '../../../assets/images/other/landings/thumb/lamd1.png'
import land1Full from '../../../assets/images/other/landings/lamd1.png'
import land2 from '../../../assets/images/other/landings/thumb/land2.png'
import land2Full from '../../../assets/images/other/landings/land2.png'
import land3 from '../../../assets/images/other/landings/thumb/land3.png'
import land3Full from '../../../assets/images/other/landings/land3.png'
import land4 from '../../../assets/images/other/landings/thumb/land4.png'
import land4Full from '../../../assets/images/other/landings/land4.png'
import land5 from '../../../assets/images/other/landings/thumb/land5.png'
import land5Full from '../../../assets/images/other/landings/land5.png'
import land6 from '../../../assets/images/other/landings/thumb/land6.png'
import land6Full from '../../../assets/images/other/landings/land6.png'
import land7 from '../../../assets/images/other/landings/thumb/land7.png'
import land7Full from '../../../assets/images/other/landings/land7.png'
/*Other End*/
/*Sites Start*/
import CosmoMain from '../../../assets/images/works/cosmo/thumb/cosmo-main.png'
import CosmoMainBig from '../../../assets/images/works/cosmo/cosmo-main.png'
import CosmoMainAll from '../../../assets/images/works/cosmo/thumb/main-all.png'
import CosmoMainAllBig from '../../../assets/images/works/cosmo/main-all.png'
import sgSite1 from '../../../assets/images/works/sg-site/thumb/screencapture-sales-generator-ru-2018-07-29-18_16_.png'
import sgSite1Big from '../../../assets/images/works/sg-site/screencapture-sales-generator-ru-2018-07-29-18_16_.png'
import sgSite2 from '../../../assets/images/works/sg-site/thumb/Screenshot_2018-07-29_18-29-42.png'
import sgSite2Big from '../../../assets/images/works/sg-site/Screenshot_2018-07-29_18-29-42.png'
import sgSite3 from '../../../assets/images/works/sg-site/thumb/screencapture-sales-generator-ru-company-2018-07-2.png'
import sgSite3Big from '../../../assets/images/works/sg-site/screencapture-sales-generator-ru-company-2018-07-2.png'
import sgSite4 from '../../../assets/images/works/sg-site/thumb/Screenshot_2018-07-29_18-31-22.png'
import sgSite4Big from '../../../assets/images/works/sg-site/Screenshot_2018-07-29_18-31-22.png'
import sdSite1 from '../../../assets/images/works/sd-site/thumb/slovo1.png'
import sdSite1Big from '../../../assets/images/works/sd-site/slovo1.png'
import sdSite2 from '../../../assets/images/works/sd-site/thumb/slovo2.png'
import sdSite2Big from '../../../assets/images/works/sd-site/slovo2.png'
import sdSite3 from '../../../assets/images/works/sd-site/thumb/slovo3.png'
import sdSite3Big from '../../../assets/images/works/sd-site/slovo3.png'
import sdSite4 from '../../../assets/images/works/sd-site/thumb/slovo4.png'
import sdSite4Big from '../../../assets/images/works/sd-site/slovo4.png'
import sdSite5 from '../../../assets/images/works/sd-site/thumb/slovo5.png'
import sdSite5Big from '../../../assets/images/works/sd-site/slovo5.png'
import sdSite6 from '../../../assets/images/works/sd-site/thumb/slovo6.png'
import sdSite6Big from '../../../assets/images/works/sd-site/slovo6.png'
import sdSite7 from '../../../assets/images/works/sd-site/thumb/slovo7.png'
import sdSite7Big from '../../../assets/images/works/sd-site/slovo7.png'
import myRemon1 from '../../../assets/images/works/myremont/thumb/myrem1.png'
import myRemon1Full from '../../../assets/images/works/myremont/myrem1.png'
import myRemon2 from '../../../assets/images/works/myremont/thumb/myrem2.png'
import myRemon2Full from '../../../assets/images/works/myremont/myrem2.png'
import myRemon3 from '../../../assets/images/works/myremont/thumb/myrem3.png'
import myRemon3Full from '../../../assets/images/works/myremont/myrem3.png'
import myRemon4 from '../../../assets/images/works/myremont/thumb/myrem4.png'
import myRemon4Full from '../../../assets/images/works/myremont/myrem4.png'
import vvsSite1 from '../../../assets/images/works/vvs-site/thumb/vvs1.png'
import vvsSite1Full from '../../../assets/images/works/vvs-site/vvs1.png'
import vvsSite2 from '../../../assets/images/works/vvs-site/thumb/vvs2.png'
import vvsSite2Full from '../../../assets/images/works/vvs-site/vvs2.png'
import vvsSite3 from '../../../assets/images/works/vvs-site/thumb/vvs3.png'
import vvsSite3Full from '../../../assets/images/works/vvs-site/vvs3.png'
import vvsSite4 from '../../../assets/images/works/vvs-site/thumb/vvs4.png'
import vvsSite4Full from '../../../assets/images/works/vvs-site/vvs4.png'
import pgSite1 from '../../../assets/images/works/pg-site/thumb/pg1.png'
import pgSite1Full from '../../../assets/images/works/pg-site/pg1.png'
import pgSite2 from '../../../assets/images/works/pg-site/thumb/pg2.png'
import pgSite2Full from '../../../assets/images/works/pg-site/pg2.png'
import pgSite3 from '../../../assets/images/works/pg-site/thumb/pg3.png'
import pgSite3Full from '../../../assets/images/works/pg-site/pg3.png'
import pgSite4 from '../../../assets/images/works/pg-site/thumb/pg4.png'
import pgSite4Full from '../../../assets/images/works/pg-site/pg4.png'
import pgSite5 from '../../../assets/images/works/pg-site/thumb/pg5.png'
import pgSite5Full from '../../../assets/images/works/pg-site/pg5.png'
import medka1 from '../../../assets/images/works/medka-site/thumb/medka1.png'
import medka1Full from '../../../assets/images/works/medka-site/medka1.png'
import medka2 from '../../../assets/images/works/medka-site/thumb/medka2.png'
import medka2Full from '../../../assets/images/works/medka-site/medka2.png'
import medka3 from '../../../assets/images/works/medka-site/thumb/medka3.png'
import medka3Full from '../../../assets/images/works/medka-site/medka3.png'
import medka4 from '../../../assets/images/works/medka-site/thumb/medka4.png'
import medka4Full from '../../../assets/images/works/medka-site/medka4.png'
import medka5 from '../../../assets/images/works/medka-site/thumb/medka5.png'
import medka5Full from '../../../assets/images/works/medka-site/medka5.png'
/*Sites End*/
import fmLogo from '../../../assets/images/brands/fm-logo.svg'
import cosmoLogo from '../../../assets/images/brands/kosmodostavka-logo.png'
import myRemontLogo from '../../../assets/images/brands/mr-logo.png'
import santehLogo from '../../../assets/images/brands/santeh-logo.png'
import sdLogo from '../../../assets/images/brands/sd-logo.png'
import sgLogo from '../../../assets/images/brands/sg-logo.svg'
import sportstyleLogo from '../../../assets/images/brands/sportstyle-logo.png'
import vvsLogo from '../../../assets/images/brands/vvs-logo.png'

export const sites = [
  {
    vvsSite1: {thumb:vvsSite1, full: vvsSite1Full},
    vvsSite2: {thumb:vvsSite2, full: vvsSite2Full},
    vvsSite3: {thumb:vvsSite3, full: vvsSite3Full},
    vvsSite4: {thumb:vvsSite4, full: vvsSite4Full},
  },
  {
    sdSite1: {thumb:sdSite1, full: sdSite1Big},
    sdSite4: {thumb: sdSite4, full: sdSite4Big},
    sdSite3: {thumb: sdSite3, full: sdSite3Big},
    sdSite2: {thumb: sdSite2, full: sdSite2Big},
    sdSite5: {thumb: sdSite5, full: sdSite5Big},
    /*sdSite6: {thumb: sdSite6, full: sdSite6Big},*/
    sdSite7: {thumb: sdSite7, full: sdSite7Big},
  },
  {
    medka1: {thumb: medka1, full: medka1Full},
    medka2: {thumb:medka2, full: medka2Full},
    /*medka3: {thumb:medka3, full: medka3Full},*/
    medka4: {thumb:medka4, full: medka4Full},
    medka5: {thumb:medka5, full: medka5Full},
  },
  {
    pgSite5: {thumb:pgSite5, full: pgSite5Full},
    pgSite2: {thumb:pgSite2, full: pgSite2Full},
    pgSite3: {thumb:pgSite3, full: pgSite3Full},
    pgSite4: {thumb:pgSite4, full: pgSite4Full},
    pgSite1: {thumb:pgSite1, full: pgSite1Full},
  },
  {
    myRemon1: {thumb:myRemon1, full: myRemon1Full},
    myRemon2: {thumb:myRemon2, full: myRemon2Full},
    myRemon3: {thumb:myRemon3, full: myRemon3Full},
    myRemon4: {thumb:myRemon4, full: myRemon4Full},
  },
  {
    cosmoMain: {thumb: CosmoMain, full: CosmoMainBig},
    cosmoMainAll: {thumb: CosmoMainAll, full: CosmoMainAllBig},
    cosmoMainAll1: {thumb: CosmoMainAll, full: CosmoMainAllBig},
    cosmoMain1: {thumb: CosmoMain, full: CosmoMainBig},
  },
  {
    sgSite1: {thumb:sgSite1, full: sgSite1Big},
    sgSite2: {thumb:sgSite2, full: sgSite2Big},
    sgSite4: {thumb:sgSite4, full: sgSite4Big},
    sgSite3: {thumb:sgSite3, full: sgSite3Big},
  },
]

export const applications = [
  {
    sgAppAnalitycs: {thumb: sgAppAnalitycs, full: sgAppAnalitycsFull},
    sgAppAnalitycs2: {thumb: sgAppAnalitycs2, full: sgAppAnalitycs2Full},
    sgAppDocuments: {thumb: sgAppDocuments, full: sgAppDocumentsFull},
    sgAppMetrix: {thumb: sgAppMetrix, full: sgAppMetrixFull},
    sgAppPm: {thumb: sgAppPm, full: sgAppPmFull},
    sgAppReports: {thumb: sgAppReports, full: sgAppReportsFull},
  },
  {
    vvsApp1: {thumb: vvsApp1, full: vvsApp1Full},
    vvsApp2: {thumb: vvsApp2, full: vvsApp2Full},
    vvsApp3: {thumb: vvsApp3, full: vvsApp3Full},
    vvsApp4: {thumb: vvsApp4, full: vvsApp4Full},
    vvsApp5: {thumb: vvsApp5, full: vvsApp5Full},
    vvsApp6: {thumb: vvsApp6, full: vvsApp6Full},
  },
  {
    todo1: {thumb: todo1, full: todo1Full},
    todo2: {thumb: todo2, full: todo2Full},
    todo3: {thumb: todo3, full: todo3Full},
    todo4: {thumb: todo4, full: todo4Full},
    todo5: {thumb: todo5, full: todo5Full},
    todo6: {thumb: todo6, full: todo6Full},
  },
]

export const other = [
  {
    land1: {thumb: land1, full: land1Full},
    land4: {thumb: land4, full: land4Full},
  },
  {
    land6: {thumb: land6, full: land6Full},
    land7: {thumb: land7, full: land7Full},
    land3: {thumb: land3, full: land3Full},
  },
  {
    gift1: {thumb: gift1, full: gift1Full},
    gift2: {thumb: gift2, full: gift2Full},
    land5: {thumb: land5, full: land5Full},
  },
]

export const brands = [
  fmLogo, sgLogo, myRemontLogo, santehLogo, vvsLogo, sdLogo, /*sportstyleLogo, */cosmoLogo
]