import React from 'react'
import NavMenuBottom from './../nav-menu/NavMenuBottom'

const Footer = ({ location }) => (
  <footer className={'footer'}>
    <div className="container">
      <div className="footer-block__left">
        <NavMenuBottom location={location} />
      </div>
      <div className="footer-block__right">
        <p className={'copyrights'}>
          © 2018, Разработано <span>Pashebor's Laba</span>
        </p>
      </div>
    </div>
  </footer>
)

export default Footer
