import React from  'react'
import Link from 'gatsby-link'

const  Paginator = ({pathContext}) => {
  const {index, first, last, pageCount } = pathContext,
        previousUrl = () => {
          const indexPos = num => num === 1 ? '' : (num).toString()

          if (index === 1) {
            return ""
          } else if(index > 1) {
            return indexPos(index - 1)
          }
        },
        nextUrl = () => {
          if(index === pageCount) {
            return ''
          } else {
            return (index + 1).toString()
          }
        },
        pagesLinks = () => {
          const links = [],
                setLink = i => {
                    if (i === index) {
                      links.push(<span key={i} className={'paginator__page paginator__page--active'}>{i}</span>)
                    } else if (i === 1)  {
                      links.push(<Link key={i} className={'paginator__page'} to={`/blog/`}>{i}</Link>)
                    } else {
                      links.push(<Link key={i} className={'paginator__page'} to={`/blog/${i}`}>{i}</Link>)
                    }
                }

          for (let i = 0; i <= pageCount; i+=1) {
            if (i > 0) {
              setLink(i)
            }
          }
          return links.map(link => link)
        }
  console.log(pathContext)
  return (
    <section className={'paginator'}>
      {first
        ?
        <span className={'paginator__prev paginator__prev--not-active'}>
          <i className={'material-icons'}>keyboard_arrow_left</i>
        </span>
        :
        <Link className={'paginator__prev'} to={`/blog/${previousUrl()}`}>
          <i className={'material-icons'}>keyboard_arrow_left</i>
        </Link>
      }
      {pagesLinks()}
      {
        last
          ?
          <span className={'paginator__next paginator__next--not-active'} >
            <i className={'material-icons'}>keyboard_arrow_right</i>
          </span>
          :
          <Link className={'paginator__next'}  to={`/blog/${nextUrl()}`}>
            <i className={'material-icons'}>keyboard_arrow_right</i>
          </Link>
      }
    </section>
  )
}

export default Paginator