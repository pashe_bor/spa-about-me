import React from 'react'
import Link from 'gatsby-link'

const BlogListItem = ({ article }) => (
  <article className={'blog-list__item'} style={{ marginBottom: 50 }}>
    <Link to={'/blog/' + article.slug}>
      <div
        className={'preview-image'}
        style={{
          backgroundImage: `url(${article.featured_media.localFile.childImageSharp.resolutions.src}`,
        }}
        title={article.title}
        alt={article.featured_media.localFile.childImageSharp.resolutions.src}
      >
        <time>
          <i className={'material-icons'}>date_range</i>
          {article.date}
        </time>
        <h3>{article.title}</h3>
        <div dangerouslySetInnerHTML={{ __html: article.excerpt }} />
      </div>
    </Link>
  </article>
)

export default BlogListItem
