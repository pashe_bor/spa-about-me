import React from 'react'
import BlogListItem from './BlogListItem'

class BlogPosts extends React.Component {
  constructor(props) {
    super(props)
    this.posts = this.props.posts
  }

  render() {
    return (
      <section className={'blog-list'}>
        {this.posts.map(post => {
          if (post.node.categories[0].name === 'Blog') {
            return <BlogListItem key={post.node.slug} article={post.node} />
          }
        })}
      </section>
    )
  }
}

export default BlogPosts
