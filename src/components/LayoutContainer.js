import React from 'react'
import Footer from './footer/Footer'
import { Parallax, ParallaxProvider } from 'react-scroll-parallax'
import PropTypes from 'prop-types'
import Header from './header'
import {bindActionCreators} from "redux"
import {connect} from "react-redux"
import ImageViewPopup from '../elements/popups/ImageViewPopup'
import {closeImagePopupAction} from "../actions";

class LayoutContainer extends React.Component{
  constructor(props) {
    super(props)
    this.children = props.data.children
    this.data = props.data.data
    this.closePopup = props.closeImagePopupAction
  }

  closeImageViewHandler(event) {
    const popupElement = event.target.classList

    if (
      popupElement.contains('popup-close') ||
      popupElement.contains('material-icons') ||
      popupElement.contains('popup-overlay')
    ) {
      this.closePopup()
    }
  }

  render() {
    return (
      <div>
        { this.props.popupState.isPopupOpen ?
          <ImageViewPopup
            closePopup={this.closeImageViewHandler.bind(this)}
            image={this.props.popupState.image}
          /> : null
        }

        <Header siteTitle={this.data.site.siteMetadata.title} location={this.props.data.location} />
        <ParallaxProvider>
          <Parallax
            offsetYMax={'680px'}
            offsetYMin={'0px'}>
            <div>
              {this.children()}
            </div>
            <Footer location={this.props.data.location} />
          </Parallax>
        </ParallaxProvider>
      </div>
    )
  }
}



export function mapStateToProps(store) {
  return {
    popupState: store.popupReducer
  }
}

export const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({closeImagePopupAction}, dispatch)
};

LayoutContainer.propTypes = {
  children: PropTypes.func,
  data: PropTypes.object,
  popupState: PropTypes.object
}

export default connect(mapStateToProps, mapDispatchToProps)(LayoutContainer)