import React from 'react'
import Link from 'gatsby-link'

const NavLinks = ({ path, scrolled }) => {
  return (
    <ul
      className={
        scrolled
          ? 'navigation-list navigation-list--scrolled'
          : 'navigation-list'
      }
    >
      <li
        className={`
        ${
          path === '/'
            ? 'navigation-list__item navigation-list__item--active'
            : 'navigation-list__item'
        }`}
      >
        <Link to={'/'}> О себе</Link>
      </li>
      <li
        className={`
        ${
          path === '/portfolio/'
            ? 'navigation-list__item navigation-list__item--active'
            : 'navigation-list__item'
        }`}
      >
        <Link to={'/portfolio/'}>Портфолио</Link>
      </li>
      <li
        className={`
        ${
          path === '/blog/'
            ? 'navigation-list__item navigation-list__item--active'
            : 'navigation-list__item'
        }`}
      >
        <Link to={'/blog/'}>Блог</Link>
      </li>
    </ul>
  )
}
export default NavLinks
