import React from 'react'

const ContactLinks = ({ scrolled, isHeader }) => {
 if (isHeader) {
   return (<ul
     className={
       scrolled
         ? 'contact-links-list contact-links-list--scrolled'
         : 'contact-links-list'
     }
   >
     <li className="contact-links-list__item contact-links-list__item--github">
       <a href={'https://github.com/Pashebor'}> </a>
     </li>
     <li className="contact-links-list__item contact-links-list__item--bitbucket">
       <a href={'https://bitbucket.org/pashe_bor/'}> </a>
     </li>
     <li className="contact-links-list__item contact-links-list__item--whatsup">
       <a href={'tg://resolve?domain=pashebor'}> </a>
     </li>
   </ul>)
 } else {
   return (<ul
     className={
       scrolled
         ? 'contact-links-list contact-links-list--scrolled'
         : 'contact-links-list'
     }
   >
     <li className="contact-links-list__item contact-links-list__item--telegram">
       <a href={'https://github.com/Pashebor'}> </a>
     </li>
     <li className="contact-links-list__item contact-links-list__item--skype">
       <a href={'https://bitbucket.org/pashe_bor/'}> </a>
     </li>
     <li className="contact-links-list__item contact-links-list__item--whatsup">
       <a href={'tg://resolve?domain=pashebor'}> </a>
     </li>
   </ul>)
 }
}

export default ContactLinks
