import React from 'react'
import Helmet from 'react-helmet'
import ContactLinks from './../components/links/ContactLinks'
import Switcher from './../components/content/Switcher'
import Work from './../components/content/Work'
import Applications from './../components/content/Applications'
import Favouite from './../components/content/Favouite'
import Resume from './../components/content/Resume'
import FlyContainer from './../elements/containers/FlyContainer'
import TitleImage from './../elements/image-elements/TitleImage'
import favicon from './../assets/images/favicon.png'
import {bindActionCreators} from "redux"
import {connect} from "react-redux"
import {openImagePopupAction} from './../actions'

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
        description
        keywords
      }
    }
  }
`

class MainPage extends React.Component {
  constructor(props) {
    super(props)
    this.openImagePopup = props.openImagePopupAction
    this.state = {
      previews: {
        applications: false,
        works: true,
        other: false,
        resume: false,
      }
    }
  }

  previewButtonsHandler(value) {
    const newState = this.state.previews

    for (let preview in this.state.previews) {
      if (preview === value) {
        newState[`${preview}`] = true
      } else {
        newState[`${preview}`] = false
      }
    }

    this.setState({ previews: newState })
  }

  imageViewHandler(image) {
    this.openImagePopup({isOpen: true, image: image})
  }

  render() {
    return (
      <div>
      <FlyContainer styles={this.props.popupState.isPopupOpen ? { zIndex: 10 } : {}}>
        <Helmet
          title={this.props.data.site.siteMetadata.title}
          meta={[
            {
              name: 'description',
              content: `${this.props.data.site.siteMetadata.description}`,
            },
            {
              name: 'keywords',
              content: `${this.props.data.site.siteMetadata.keywords}`,
            },
            { name: 'theme-color', content: '#881F62'},
            { name: 'background', content: '#881F62'},
            { name: 'lang', content: 'ru'}
          ]}
          link={[
            { rel: 'shortcut icon', type: 'image/png', href: `${favicon}` }
          ]}
        />
        <TitleImage />
        <h1 className={'main-header'}>Павел Демьянов</h1>
        <h5 className={'main-subheader'}>web-developer</h5>
        <ContactLinks />
        <div className="content">
          <p>
            Привет! Мое имя Павел Демьянов. <br /> Я являюсь веб разработчиком
            уже около 5 лет. До того как начать разрабатывать сайты и различного
            рода вэб сервисы я был увлечен разработкой мобильных и
            desktop-приложений на java/java-android. На данный момент являюсь
            ведущим разработчиком (Team-lead). Общий опыт программирования около
            10 лет.
          </p>
          <Switcher
            state={this.state}
            previewSwitcher={this.previewButtonsHandler.bind(this)}
          />
          {this.state.previews.works ? (
            <Work popupAction={this.imageViewHandler.bind(this)} />
          ) : null}
          {this.state.previews.applications ? (
            <Applications popupAction={this.imageViewHandler.bind(this)} />
          ) : null}
          {this.state.previews.other ? (
            <Favouite popupAction={this.imageViewHandler.bind(this)} />
          ) : null}
          {this.state.previews.resume ? <Resume /> : null}
        </div>
      </FlyContainer>
      </div>
    )
  }
}

export function mapStateToProps(store) {
  return {
    popupState: store.popupReducer
  }
}

export const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({openImagePopupAction}, dispatch)
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage)
