import React from 'react'
import Helmet from 'react-helmet'
import FlyContainer from '../elements/containers/FlyContainer'
import TitleImage from '../elements/image-elements/TitleImage'
import WorksList from '../components/portfolio/WorksList'
import favicon from './../assets/images/favicon.png'

export const query = graphql`
  query PortfolioQuery {
    site {
      siteMetadata {
        keywords
        portfolio {
          title
          description
        }
      }
    }
    allMarkdownRemark{
        edges {
          node {
            frontmatter {
              author
              description
              tags
              appName
              section
              imagesThumb{
                childImageSharp{
                  resolutions(width: 400, quality: 50) {
                   src
                  }
                }
              }
              imagesFull{
                childImageSharp{
                  resolutions(width: 800, quality: 50){
                   src
                  }
                }
              }
            }
          }
        }
      }
  }
`

class Portfolio extends React.Component {
  render() {
    const data = this.props.data.site.siteMetadata,
          works = this.props.data.allMarkdownRemark.edges
    return (
        <FlyContainer>
          <Helmet
            title={data.portfolio.title}
            meta={[
              { name: 'description', content: data.portfolio.description },
              { name: 'keywords', content: data.keywords },
              { name: 'theme-color', content: '#881F62'},
              { name: 'background', content: '#881F62'},
              { name: 'lang', content: 'ru'}
            ]}
            link={[
              { rel: 'shortcut icon', type: 'image/png', href: `${favicon}` }
            ]}
          />
          <TitleImage type={'portfolio'} />
          <h1 className={'main-header'}>Портфолио</h1>
          <WorksList works-data={works}/>
        </FlyContainer>
    )
  }
}

export default Portfolio
