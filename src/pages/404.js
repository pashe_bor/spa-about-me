import React from 'react'
import Helmet from 'react-helmet'
import Link from 'gatsby-link'
import TitleImage from '../elements/image-elements/TitleImage'
import FlyContainer from '../elements/containers/FlyContainer'
import favicon from './../assets/images/favicon.png'

export const notFoundQuery = graphql`
  query notFoundQuery {
    site {
      siteMetadata {
        title
        description
        keywords
      }
    }
  }
`

const NotFoundPage = (props) => (
    <FlyContainer style={{minHeight: '600px'}}>
      <Helmet
        title={'404 | Страница не найдена!'}
        meta={[
          { name: 'description', content: props.data.site.siteMetadata.description },
          { name: 'keywords', content: props.data.site.siteMetadata.keywords },
          { name: 'theme-color', content: '#881F62'},
          { name: 'background', content: '#881F62'},
          { name: 'lang', content: 'ru'}
        ]}
        link={[
          { rel: 'shortcut icon', type: 'image/png', href: `${favicon}` }
        ]}
      />
      <TitleImage type={'404'} />
      <h1 className={'not-found-header'} data-text="404">404</h1>
      <h3 className="main-header">Такой страницы не сущесвует</h3>
      <div className="content content--404">
        <p>Перейдите на <Link to={'/'}>главную</Link> страницу.</p>
        <p>Почитайте статьи в <Link to={'/blog'}>блоге</Link>.</p>
      </div>
    </FlyContainer>
)

export default NotFoundPage
