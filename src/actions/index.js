import {IMAGE_POPUP_OPEN, IMAGE_POPUP_CLOSE} from './../constants'


export const openImagePopupAction = popupData => {
  return {
    type: IMAGE_POPUP_OPEN,
    payload: popupData
  }
}

export const closeImagePopupAction = () => {
  return {
    type: IMAGE_POPUP_CLOSE,
    payload: false
  }
}
