const _ = require(`lodash`);
const Promise = require(`bluebird`);
const path = require(`path`);
const slash = require(`slash`);
const createPaginatedPages = require('gatsby-paginate');

// Implement the Gatsby API “createPages”. This is
// called after the Gatsby bootstrap is finished so you have
// access to any information necessary to programmatically
// create pages.
// Will create pages for WordPress pages (route : /{slug})
// Will create pages for WordPress posts (route : /post/{slug})

const postsQuery = `
query blogQuery {
    allWordpressPost {
      edges {
        node {
          id
          title
          excerpt
          slug
          author {
            authored_wordpress__wp_comments {
              content
            }
          }
          date(formatString: "DD.MM.YYYY")
          featured_media {
             localFile {
                childImageSharp {
                  resolutions(width: 600, height: 300, quality: 53) {
                    src
                    srcSet
                    width
                    height
                  }
                }
             }
          }
          categories {
            name
          }
        }
      }
    }
  }
`

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;
  
  return new Promise((resolve, reject) => {
    // Templates
    const postTemplate = path.resolve("./src/templates/post.js");
    
    resolve(
      graphql(postsQuery).then(result => {
        if (result.errors) reject(result.errors)
        
        // Blog detail
        const blog = result.data.allWordpressPost.edges
        
        createPaginatedPages({
          edges: blog,
          createPage: createPage,
          pageTemplate: "src/templates/blog.js",
          pageLength: 12,
          pathPrefix: "blog"
        })
        
        blog.map(edge => {
          createPage({
            path: `/blog/${edge.node.slug}/`,
            component: slash(postTemplate),
            context: {
              id: edge.node.id,
            },
          });
        })
        
      })
    )
  });
};