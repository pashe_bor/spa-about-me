const autoprefixer = require('autoprefixer');

module.exports = {
  siteMetadata: {
    siteUrl: 'https://plaba.ru',
    title: 'Павел Демьянов | О себе',
    keywords: 'Павел Демьянов, Web-разработчик, Web developer, Front-end developer, Front-end разработчик',
    description: 'Павел Демьянов - разрабатывает сайты, вэб-приложения, мобильные приложения',
    portfolio: {
      title: 'Павел Демьянов | Портфолио',
      description: 'Портфолио Павла Демьянова'
    },
    blog: {
      title: 'Павел Демьянов | Блог',
      description: 'Блог Павла Демьянова  '
    }
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: `sitemap.xml`,
        query: `{
          site {
            siteMetadata {
              siteUrl
            }
          }

          allSitePage {
            edges {
              node {
                path
              }
            }
          }
        }`
      }
    },
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        host: 'https://plaba.ru',
        sitemap: 'https://plaba.ru/sitemap.xml',
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    {
      resolve: `gatsby-plugin-postcss-sass`,
      options: {
          postCssPlugins: [autoprefixer({add: true, browsers: ['last 4 version']})],
          precision: 8, // SASS default: 5
      },
  },{
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/markdown`,
        name: "portfolio",
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-prismjs",
            options: {},
          },
        ]
      }
    },
    {
    resolve: "gatsby-source-wordpress",
    options: {
      /*
       * The base URL of the Wordpress site without the trailingslash and the protocol. This is required.
       * Example : 'gatsbyjsexamplewordpress.wordpress.com' or 'www.example-site.com'
       */
      baseUrl: "pashebor.wordpress.com",
      // The protocol. This can be http or https.
      protocol: "https",
      // Indicates whether the site is hosted on wordpress.com.
      // If false, then the assumption is made that the site is self hosted.
      // If true, then the plugin will source its content on wordpress.com using the JSON REST API V2.
      // If your site is hosted on wordpress.org, then set this to false.
      hostingWPCOM: true,
      // If useACF is true, then the source plugin will try to import the Wordpress ACF Plugin contents.
      // This feature is untested for sites hosted on Wordpress.com.
      // Defaults to true.
      useACF: false,
      auth: {
        // If auth.user and auth.pass are filled, then the source plugin will be allowed
        // to access endpoints that are protected with .htaccess.
        htaccess_user: "pashebor@gmail.com",
        htaccess_pass: "ltvmzyjd90",
        htaccess_sendImmediately: true,
      
        // If hostingWPCOM is true then you will need to communicate with wordpress.com API
        // in order to do that you need to create an app (of type Web) at https://developer.wordpress.com/apps/
        // then add your clientId, clientSecret, username, and password here
        wpcom_app_clientSecret:
          "kQXPHb0sbU2K3DcthOKJ0A5HUcWzmg5Jo2zTIF8cZXszHT3OnucCqiJEstWQul7x",
        wpcom_app_clientId: "58974",
        wpcom_user: "pashebor@gmail.com",
        wpcom_pass: "ltvmzyjd90",
      },
      // Set verboseOutput to true to display a verbose output on `npm run develop` or `npm run build`
      // It can help you debug specific API Endpoints problems.
      verboseOutput: false,
      // Set how many pages are retrieved per API request.
      perPage: 100,
      // Search and Replace Urls across WordPress content.
      /*searchAndReplaceContentUrls: {
        sourceUrl: "https://pashebor.wordpress.com",
        replacementUrl: "https://plaba.ru",
      },*/
      // Set how many simultaneous requests are sent at once.
      concurrentRequests: 10,
      // Exclude specific routes using glob parameters
      // See: https://github.com/isaacs/minimatch
      // Example:  `["/*/*/comments", "/yoast/**"]` will exclude routes ending in `comments` and
      // all routes that begin with `yoast` from fetch.
      excludedRoutes: ["/*/*/comments", "/yoast/**"],
      // use a custom normalizer which is applied after the built-in ones.
      normalizer: function({ entities }) {
        return entities;
      },
    },
  }],
}
